.PHONY: build
build:
	./src/md2html.sh

.PHONY: clean
clean:
	rm *.html
