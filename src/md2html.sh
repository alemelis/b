#!/bin/bash

for f in md/*.md
do
    name=$(basename "$f")
    pandoc $f -s --highlight-style zenburn -c src/b.css -o "${name%.*}".html
done
