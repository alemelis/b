### run interactive image

```console
docker run -it --entrypoint /bin/bash <image>
```

---

### copy from/to container

```console
docker ps <src> <dst>
```

you need to use the container name, not the image one

---

### gcloud login

```console
gcloud auth login
gcloud docker -a
```
