---
title: Stuff I read
---

- Matthew Ström: [How to pick the least wrong colors](https://matthewstrom.com/writing/how-to-pick-the-least-wrong-colors/). Computing a color palette with simulated annealing.

# Python

- [Reduce your Python program's memory usage with Fil](https://pythonspeed.com/fil/). Memory profiling with flame graphs.
