---
title: GTA5 modding
---

### Setup

- Install GTA5 through Steam.

- Download [ScritpHookV](http://www.dev-c.com/gtav/scripthookv/).
- Copy the content of `bin` folder into your local game directory (`C:\Program Files (x86)\Steam\steamapps\common\Grand Theft Auto V`). This may require a previous version of the game `.exe` (the game will not run and show an error message). In that case, download a previous version of the executable[^0] through Steam console[^1]:
    - go to [steam://open/console](steam://open/console);
    - copy the relevant (closest to ScriptHookV latest version) manifest ID from [SteamDB](https://steamdb.info/depot/271591/manifests/);
    - run in the console `download_depot 271590 271591 <manifest ID>`
    - go to your local `steam/steamapps/content/app_271590/depot_271591` and copy `GTA5.exe` and `GTAVLauncher.exe` into the main game folder (just in case, backup the original files).


Now we can install [mods](https://www.gta5-mods.com). Actually a cheat trainer is already installed: launch the game and press `F4`, a menu should appear on the top left (use the numpad to navigate: `2`, `4`, `8`, `6` as arrows and `5` to enter), have fun.

#### Useful mods

- [LuaPlugin-GTAV](https://github.com/JayMontana36/LuaPlugin-GTAV) script the game in Lua, not C++/#. For example, the following script (from the [wiki](https://github.com/JayMontana36/LuaPlugin-GTAV/wiki/Overview#first-script)) continuosly prints to console

```lua
local print = print -- Localize the print function for faster access (optional)
return {
    loop =  function()
                print("Hello World!")
            end
}
```

- skip loading stuff

### Repositories and mods

- [GTAVDeveloperConsole](https://github.com/Dakota628/GTAVDeveloperConsole) run C# code from console.
- [JTA-Mods](https://github.com/fabbrimatteo/JTA-Mods) used to create the dataset for [Learning to Detect and Track Visible and Occluded Body Joints in a Virtual World]( https://arxiv.org/abs/1803.08319). Brief dev guide provided.
- [GCC-CL](https://github.com/gjy3035/GCC-CL) tool for generating synthetic [crowd](crowds.html) image datasets. 
- [GTAVisionExport](https://github.com/umautobots/GTAVisionExport) tool to export full segmentations. Dev discussion [here](https://github.com/umautobots/GTAVisionExport/issues/1).
- [playing-for-data](https://bitbucket.org/visinf/projects-2016-playing-for-data/src/master/) segmentation.
- [enricomeloni/GTA5_Mods](https://github.com/enricomeloni/GTA5_Mods) code for people (other objects?) bounding boxes ([blog](https://enricomeloni.github.io/project/daeny/)).
- [DeepGTAV](https://github.com/aitorzip/DeepGTAV) transform GTAV into a vision-based self-driving car research environment (like [CarlaSim](https://carla.org)). Seems like it can show/export 3d bboxes (see [this issue](https://github.com/aitorzip/DeepGTAV/issues/19)). Duscussion about flickering boxes [here](https://github.com/aitorzip/DeepGTAV/issues/61). Also, check the refactored version, [DeeperGTAV](https://github.com/gdpinchina/DeeperGTAV).
- [GTA-V-Camera-Tracker](https://github.com/awaelchli/GTA-V-Camera-Tracker) record and export camera pose.
- [G2D](https://github.com/dadung/G2D) record video and camera pose.
- [gta5_vision-data-extractor](https://github.com/taka-mochi/gta5_vision-data-extractor) extract 2d bboxes.
- [Lidar_gta_5](https://github.com/niqbal996/Lidar_gta_5) cloud points similar to KITTI.
- Get dimensions of car [forum link](https://forums.gta5-mods.com/topic/30068/get-dimensions-of-entity-like-cars/2)
- [L.S Traffic](https://www.gta5-mods.com/misc/l-s-traffic) increases traffic!

### Refs

[^0]: How to download old .exes after Update (Steam) [src](https://forums.gta5-mods.com/topic/18253/how-to-download-old-exes-after-update-steam/2)
[^1]: Steam Console [src](https://steamcommunity.com/sharedfiles/filedetails/?id=873543244)
