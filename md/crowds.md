---
title: Crowd detection
---

In [Learning from Synthetic Data for Crowd Counting in the Wild](https://gjy3035.github.io/GCC-CL/) they used GTA5 (they released code and images) to generate 15k+ images. They then used some sort of GANs to make them realistic (dataset [here](http://share.crowdbenchmark.com:2443/home/Translation_Results))

![](https://gjy3035.github.io/GCC-CL/images/SFCN.jpg)

Finally they trained a network (SFCN) to return the heads density estimate in the image; from this the heads count is computed. Pre-trained (pytorch from 2019) model [here](https://github.com/gjy3035/GCC-SFCN) (it uses a ResNet101 backbone).

Their model works fine on our images, however I had to apply few changes to the code to run with python3x (and on CPU).
