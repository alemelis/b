### trim

```console
ffmpeg -i movie.mp4 -ss 00:00:03 -t 00:00:08 -async 1 trim.mp4
```

where `-ss` is the start time and `-t` is the new video duration

---

### avi2mp4

```console
ffmpeg -i movie.avi -c:v copy -c:a copy -y movie.mp4
```

---

### 4x4 mosaic

[source](http://trac.ffmpeg.org/wiki/Create%20a%20mosaic%20out%20of%20several%20input%20videos)

```console
ffmpeg -i 1.avi -i 2.avi -i 3.avi -i 4.avi \
        -filter_complex "nullsrc=size=640x480 [base]; \
		[0:v] setpts=PTS-STARTPTS, scale=320x240 [upperleft]; \
		[1:v] setpts=PTS-STARTPTS, scale=320x240 [upperright]; \
		[2:v] setpts=PTS-STARTPTS, scale=320x240 [lowerleft]; \
		[3:v] setpts=PTS-STARTPTS, scale=320x240 [lowerright]; \
		[base][upperleft] overlay=shortest=1 [tmp1]; \
		[tmp1][upperright] overlay=shortest=1:x=320 [tmp2]; \
		[tmp2][lowerleft] overlay=shortest=1:y=240 [tmp3]; \
		[tmp3][lowerright] overlay=shortest=1:x=320:y=240" \
	    -c:v libx264 output.mkv
```
