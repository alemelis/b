---
title: Books
---

- D. Kushner [Masters of Doom](https://www.goodreads.com/book/show/222146.Masters_of_Doom). The story of John Carmak, John Romero and Id Software from Captain Keen to Quake III.
