---
title: Fizz Buzz golf
---

# Description

Print the numbers from 1 to 100 inclusive, each on their own line. If, however, the number is a multiple of three then print __Fizz__ instead, and if the number is a multiple of five then print __Buzz__. If multiple conditions hold true then all replacements should be printed, for example 15 should print __FizzBuzz__.

# Solution

Just following the prompt we get something like

```julia
for n=1:100
  if n%3==0 && n%5>0
    println("Fizz")
  elseif n%3>0 && n%5==0
    println("Buzz")
  elseif n%3==0 && n%5==0
    println("FizzBuzz")
  else
    println(n)
  end
end
```

for a grand total of `179 bytes` (note the 2 spaces indent instead of the usual 4).
    
Now, this is straightforward to shorten to `128 bytes`

```julia
for n=1:100
  s=""
  if n%3<1
    s*="Fizz"
  end
  if n%5<1
    s*="Buzz"
  end
  if length(s)<1
    s=n
  end
  println(s)
end
```

The idea is to store the output in the variable `s`.
Its value is updated by two divisibility checks (`n%3<1` and `n%5<1`)[^0] where the strings `Fizz` and `Buzz` are appended (`*` operator) independently.
Eventually, if `s` hasn't been updated (i.e. `length(s)<1`), its value is replace with `n`.

Getting rid of spaces and newlines brings the solution to `99 bytes`

```julia
for n=1:100
s=""
if n%3<1
s*="Fizz"
end
if n%5<1
s*="Buzz"
end
if length(s)<1
s=n
end
println(s)end
```

Following, the conditionals can be inlined (`87 bytes`)

```julia
for n=1:100
s=""
n%3<1&&(s*="Fizz")
n%5<1&&(s*="Buzz")
length(s)<1&&(s=n)
println(s)end
```

where the `&&` (`AND`) operator is used to replace[^1] the `if` statement (note the need for parenthesis around `s` updates).

Let's now try to remove the lengthy conditionals. 
For this purpose we use the `^` operator whose dyadic string-integer method aliases the `repeat` function (i.e. `"ah"^3` returns `ahahah`).
Furthermore, the same operator can be used with booleans where `"ah"^false` returns an empty string.
We can then build a single string for our problem as (`76 bytes`)

```julia
for n=1:100
s="Fizz"^(n%3<1)*"Buzz"^(n%5<1)
length(s)<1&&(s=n)
println(s)end
```

We can now bring the string composition inside `println` to reduce the line count (but scoring one byte more, `77 bytes`)

```julia
for n=1:100
println(length((s="Fizz"^(n%3<1)*"Buzz"^(n%5<1);))<1 ? n : s)
end
```

Note the extra parenthesis and the `;` inside `length`.
We also replace the `&&` conditional with a ternary operator (`? :`) call to merge the three conditions.

The next step is to replace `length`.
We can use the `>` comparison operator between `s` and the empty string `""` (`69 bytes`)

```julia
for n=1:100
println((s="Fizz"^(n%3<1)*"Buzz"^(n%5<1))>"" ? s : n)
end
```

Also the loop can be in-lined by using the `.|>` operator (`65 bytes`)

```julia
1:100 .|>n->println((s="Fizz"^(n%3<1)*"Buzz"^(n%5<1))>"" ? s : n)
```

This is already one byte shorter than the best solution posted on [julialang discourse](https://discourse.julialang.org/t/please-critique-my-fizzbuzz/30987/38?u=alemelis) (`66 bytes`)

```julia
1:100 .|>n->(f=n%3<1;b=n%5<1;println(f|b ? "Fizz"^f*"Buzz"^b : n))
```

Here the conditions are stored into two variables `f` and `b`, and at comparison time, the bitwise `OR` operator `|` is used in the conditional, and `s` is not defined anymore.
We can re-use these ideas to reach `64 bytes`

```julia
1:100 .|>i->println((f=n%3<1)|(b=n%5<1) ? "Fizz"^f*"Buzz"^b : i)
```

which for the moment is the best I have, but the current record on code.golf is [`59 bytes`](https://code.golf/rankings/holes/fizz-buzz/julia/bytes).
The annoying part of this solution is the ternary operator `c ? t : f` which adds four spaces. Getting rid of those we'd get to `60 bytes` which is very close to the best solution...

# More (longer) onliners

```julia
1:100 .|>i->println((n="Fizz"^(i%3<1)*"Buzz"^(i%5<1))>"" ? n : i) 
1:100 .|>i->[i,"Fizz"^(f=i%3<1)*"Buzz"^(b=i%5<1)][f|b+1]|>println
1:100 .|>n->println([n,"Fizz"^(f=n%3<1)*"Buzz"^(b=n%5<1)][f|b+1])
println.(1:100 .|>i->(f=i%3<1)|(b=i%5<1) ? "Fizz"^f*"Buzz"^b : i)
println.((f=i%3<1)|(b=i%5<1) ? "Fizz"^f*"Buzz"^b : i for i=1:100)
1:100 .|>i->(f=i%3<1;b=i%5<1;println(f|b ? "Fizz"^f*"Buzz"^b : i))
1:100 .|>i->"Fizz"^(f=i%3<1)*"Buzz"^(b=i%5<1)*"$i"^!(f|b)|>println
for i=1:100;f=i%3<1;b=i%5<1;println(f|b ? "Fizz"^f*"Buzz"^b : i)end
1:100 .|>i->"Fizz"^(f=i%3<1)*"Buzz"^(b=i%5<1)*"$i"^(!f&!b)|>println
1:100 .|>i->[i,"Fizz"^(f=i%3<1)*"Buzz"^(b=i%5<1)][1+(f|b)]|>println
i<n=102>i+1<println((f=1>i%3)|(b=1>i%5) ? "Fizz"^f*"Buzz"^b : i);1<0
for i=1:100 x="Fizz"^(i%3<1)*"Buzz"^(i%5<1);println(x>"" ? x : i)end
1:1e2.|>i->println((f=i%3<1)|(b=i%5<1) ? "Fizz"^f*"Buzz"^b : Int(i))
f(i)=println((n="Fizz"^(i%3<1)*"Buzz"^(i%5<1))>"" ? n : i);f.(1:100)
1:100 .|>i->println(("Fizz"^(f=i%3<1)*"Buzz"^(b=i%5<1)*"$i"^(!f&!b)))
1:100 .|>i->println([i,"Fizz","Buzz","FizzBuzz"][1+2(i%5<1)+(i%3<1)])
/ =gcd;1:100 .|>n->println((a="FizzBuzz"[7-2(3/n):3+5/n])>"" ? a : n)
1:100 .|>i->(n="Fizz"^(i%3<1)*"Buzz"^(i%5<1);println([i,n][2^(n>"")]))
1:100 .|>i->println(split("$i Fizz Buzz FizzBuzz")[1+2(i%5<1)+(i%3<1)])
1:100 .|>i->[i,"$("Fizz"^(f=i%3<1))$("Buzz"^(b=i%5<1))"][f|b+1]|>println
[[x,"Fizz","Buzz","FizzBuzz"][1+2*(x%5<1)+(x%3<1)] for x=1:100].|>println
for i=1:100 f,b=.≈((i%3,i%5),0);println("Fizz"^f*"Buzz"^b*"$i"^!(f|b))end
1:100 .|>i->println(split("Fizz Buzz $i").^[(v=i.%[3,5].<1;);!any(v)]...)
f(i)=(f=i%3<1;b=i%5<1;"Fizz"^f*"Buzz"^b*"$i"^!(f|b));f.([1:100;]).|>println
i<N=102>i+1<[N;(f=1>i%3)|(b=1>i%5) ? "Fizz"^f*"Buzz"^b : i]~println.(N);2<1
1:100 .|>i->(f=i.%[3,5].<1;println(join(["Fizz","Buzz","$i"][[f;sum(f)<1]])))
i=0;while i<100;i+=1;println((n="Fizz"^(i%3<1)*"Buzz"^(i%5<1))>"" ? n : i)end
```


[^0]: Short divisibility check: `n%d==0` becomes `n%d<1`
[^1]: Short circuit `&&`: `if cond; expr;end` becomes `cond&&expr`
