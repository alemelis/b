---
title: Games
---

## Playing

- Thimbleweed Park (macOS) by [Ron Gilbert](https://grumpygamer.com) #point-and-click #adventure #pixel-art
Mystery story with Twin Peaks vibes. Novel(?) gameplay where multiple characters can be used at the same time to solve puzzles. I'm glad there's an in-game hint system; I don't miss looking for hints on the internet. Also welcomed the choice of giving each character a to-do list, it makes easy to pick-up the game after a while.

- Return of the Obra Dinn (macOS) by Lucas Pope #dithering #puzzle #mistery #first-person #1bit #unity #dev-log
Pretty simple gameplay, will I manage to reach the end before boring out? I actually reached the end in ~3hrs, but without solving all the 60+ questions. I guess, that doesn't count as finishing the game. The 1bit rendering is quite impressive. Enjoying reading the [dev log](https://forums.tigsource.com/index.php?topic=40832.0) so far.

## Backlog

- GTAV (PC)
- Firewatch (macOS)
- Monkey Island Collection (PC only) by Ron Gilbert, Dave Grossman and Tim Schafer at LucasArts

## Whishlist

- Lake (PC only)
- Death Stranding (PC only)
- Narita Boy
- [Return to Monkey Island](https://store.steampowered.com/app/2060130/Return_to_Monkey_Island/) (sometimes on 2022 but PC only?!?)

## Finished

- [Splinter Cell](https://store.steampowered.com/app/13560/Tom_Clancys_Splinter_Cell/) (PC) #stealth #third-person
A delight from start to finish. To be played again.

- [Crimson: Steam Pirates](https://bungie.fandom.com/wiki/Crimson:_Steam_Pirates) (iOS) #turn-based #strategy #pirates
Played several times over the years, bought DLCs. Not available anymore on the AppStore, but would love to play again. For a while I entertained the idea of implementing something with the same gameplay ideas.

- SUPERHOT & MIND CONTROL DELETE (Linux) #firt-person #low-poly #bullet-time #puzzle
Building up on bullet-time game mechanic a-la Max Payne (possibly better) but in first-person. It's more a puzzle game than a first-person shooter as you have to carefully adjust your movements to beat levels.

- Knights of the Old Republic (iOS) #rpg #starwars
Started several times, finished once. Spent too much time on the initial levels, rushed the end and got lucky with the final boss. 

- [Papers, Please](https://papersplea.se) (iOS) by Lucas Pope #puzzle #pixel-art
There are lots of possible endings (20+?); played 2-3 times (~1 hr/run). Will play again in the future.

- [Turmoil](https://store.steampowered.com/app/361280/Turmoil/) (macOS) #puzzle #game-maker-2
Completed normal mode at the second run (8 hrs in total) and unlocked hard mode. Fun, but not enough to restart the campaign. 

## Played ad-nauseam, not going to finish

- The Incredible Machine
- Worms 2

- Age of Empires I + Rome expansion & II

- Gran Prix 2
- Need for Speed: Hot Pursuit & Underground

- NBA '95
- FIFA '98

- Tomb Raider 2

- The Sims
- Caesar 3
- Theme Hospital
- Sim City 1 & 2

- GTA3 & Vice City

- Call of Duty (multiplayer)
- Star Wars Battlefront (Classic) 1 & 2
- Star Wars Rogue Squadron 3D (LucasArts)

- [Door Kickers: Action Squad](https://store.steampowered.com/app/686200/Door_Kickers_Action_Squad/) (iOS). #pixel-art #2d
Getting harder and harder. You need to master different gameplay tattics when changing character. Spent so much time trying to score three stars for each level. Possibly not all the levels can be done with the same character. Great pulp pixel art.

- [Dead Cells](https://deadcells.com) (iOS). #pixel-art #2d #haxe #generative
Super fun but impossible (for me) to finish. Collected the six runes and a good number of templates/artifacts. Don't have motivation to keep playing or buying DLCs, but will re-play in the future. Great pixel art, animations style remembered me of the original Prince of Persia. The levels are [pseudo-randomly generated](https://deepnight.net/tutorial/the-level-design-of-dead-cells-a-hybrid-approach/) at each run.

- [Super Stickman Golf](https://noodlecake.com/games/super-stickman-golf/) (iOS). #sport
Free to play and challenging. Good to have for idle time and/or share a quick gaming session.

## Enjoyed for a little while

- StarCraft
- Age of Mythology
- Impossible Creatures

- Commando

- Rollercoaster Tycoon
- Factorio

- Unreal Tournament
- Half Life (multiplayer mod)
- Serious Sam

- Tropico (PC) #isometric #strategy #simulation
 Basically Caesar but on a tropical island.

- Hitman
- Max Payne
- Star Wars Jedi Knight - Jedi Academy

## Meh

- [Space Marshals](https://en.wikipedia.org/wiki/Space_Marshals) (iOS) #3rd-person #isometric
Nice stealth gameplay mechanics. At first it looks like Commando, even though you control only a single character. After the first chapter the game doesn't evolve, I stuck with the same strategy level after level. I stopped playing when the only enemy on the level become an annoying earth diffing spider. Didn't like the horror twist, stopped playing; shame because there are three games in the franchise. 

- [Ministry of Broadcast](https://store.steampowered.com/app/874040/Ministry_of_Broadcast/) (iOS, demo) #pixel-art #game-maker-2
Amazing pixel art. Pretty boring gameplay: 2D platformer, not great controls (on tablet at least). Possibly good story.

- [Despotism 3k](https://store.steampowered.com/app/699920/Despotism_3k/) (iOS) #pixel-art #puzzle #unity
Witty text but bland gameplay. You simply have to continuosly de/allocate resorces to five different machines. Machines can be upgraded to produce/consume more resources. Survive 25 days to win. Extremely difficult, feels like a waste of time after few minutes of playing. Whish it could be that addictive, not for me.
