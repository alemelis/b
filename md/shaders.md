# Shader

A shader is a single function applied to all the screen pixels in parallel (usually by a GPU, hence super fast)[^0]. The pixel color depends on the pixel coordinates

$$
color = color(p.x, p.y).
$$

Shaders are written in GLSL (openGL Shading Language) and we can play with it on [shadertoy](https://www.shadertoy.com).

### References

[^0]: The Book of Shaders [[src](https://thebookofshaders.com)]

